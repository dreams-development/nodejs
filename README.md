# Nodejs
## Articles
### Common
- [Why the Hell Would You Use Node.js](https://medium.com/the-node-js-collection/why-the-hell-would-you-use-node-js-4b053b94ab8e)
- [Node.js Child Processes: Everything you need to know](https://medium.freecodecamp.org/node-js-child-processes-everything-you-need-to-know-e69498fe970a)
- [Node.js Best Practices](https://github.com/i0natan/nodebestpractices)
- [What you should know to really understand the Node.js Event Loop](https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c)
- [Understanding Node.js Event-Driven Architecture](https://medium.freecodecamp.org/understanding-node-js-event-driven-architecture-223292fcbc2d)
- [Node.js Streams: Everything you need to know](https://medium.freecodecamp.org/node-js-streams-everything-you-need-to-know-c9141306be93)
- [Node.js Child Processes: Everything you need to know](https://medium.freecodecamp.org/node-js-child-processes-everything-you-need-to-know-e69498fe970a)
- [Scaling Node.js Applications](https://medium.freecodecamp.org/scaling-node-js-applications-8492bd8afadc)
- [Node.js V8 internals: an illustrative primer](https://codeburst.io/node-js-v8-internals-an-illustrative-primer-83766e983bf6)
- [You don’t know Node](https://medium.com/@samerbuna/you-dont-know-node-6515a658a1ed), [[RUS]](https://habr.com/company/ruvds/blog/341646/)
- [Вы наверное шутите, мистер Дал, или почему Node.js — это венец эволюции веб-серверов](https://habr.com/post/108241/)
- [How to create a real-world Node CLI app with Node](https://medium.freecodecamp.org/how-to-create-a-real-world-node-cli-app-with-node-391b727bbed3)

### Desing API
- [RESTful API Designing guidelines — The best practices](https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9)
- [The Ultimate Guide to API Design](https://blog.qmo.io/ultimate-guide-to-api-design/?utm_source=forwebdev_vk&utm_medium=announcement&utm_campaign=polnoe-rukovodstvo-po-dizaynu-api-crud)

### Tools
- [Awesome Node.js & Libraries](https://github.com/sindresorhus/awesome-nodejs)

### Mongoose
- [Introduction to Mongoose for MongoDB](https://medium.freecodecamp.org/introduction-to-mongoose-for-mongodb-d2a7aa593c57)

### GraphQL
- [REST APIs are REST-in-Peace APIs. Long Live GraphQL.](https://medium.freecodecamp.org/rest-apis-are-rest-in-peace-apis-long-live-graphql-d412e559d8e4)

